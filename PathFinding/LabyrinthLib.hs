 module LabyrinthLib
 (
 -- Labyrinth2D
 Square(Open,Blocked),
 LabyrinthRow,
 Pos2D,
 Path2D,
 Labyrinth(Labyrinth),
 findPathInLabyrinth,
 
 -- Labyrinth IO
 readLabyrinthFromFile,
 
 -- Coordinate IO
 selectSquare,
 selectStart,
 selectEnd,
 squareToChar,
 ) where


import System.Environment
import Data.Char
import PathFinding
import Control.Monad
import Control.Applicative


{-------------------------------------------------------------------
    Labyrinth2D:
    Functions and data types for creating/navigating a 2D labyrinth.
 -------------------------------------------------------------------
-}
data Square = Open | Blocked
              deriving (Eq,Show)

type LabyrinthRow = [Square]
type Pos2D = (Int,Int)
type Path2D = [Pos2D]

data Labyrinth = Labyrinth [LabyrinthRow]
                 deriving (Eq,Show)


isSquareFree :: Pos2D -> Labyrinth -> Bool
isSquareFree (row,col) (Labyrinth xs) | (row < 0 || row >= length xs) = False
                                      | (col < 0 || col >= length (xs!!row) ) = False
                                      | otherwise = ((xs !! row) !! col) == Open


getNeighbors2D :: Pos2D ->[Pos2D]
getNeighbors2D (x,y) = [(x+1,y),(x-1,y),(x,y+1),(x,y-1)]
    
possibleMoves :: Labyrinth -> Pos2D -> [Pos2D]
possibleMoves labyrinth curPos = filter isSquareFree' . getNeighbors2D $ curPos
                                     where isSquareFree' pos = isSquareFree pos labyrinth
                                     
findPathInLabyrinth :: Labyrinth -> Pos2D -> Pos2D -> [Pos2D]
findPathInLabyrinth labyrinth start end = bfs start end (\pos -> possibleMoves labyrinth pos)   
                                     
                                     
{-------------------------------------------------------------------
    Labyrinth IO:
    Functions for importing a labyrinth from a text file.
 -------------------------------------------------------------------
-}

openSq     = '^'    -- '^' represents a free square
blockedSq  = '#'    -- '#' represents an obstacle

charToSquare c = if c==openSq then Open else Blocked
squareToChar s = if s==Open then openSq else blockedSq

strToLabyrinthRow str = map charToSquare str

readLabyrinthFromFile :: String -> IO Labyrinth
readLabyrinthFromFile filename = do
    content <- readFile filename
    let labyrinth =  Labyrinth $ map strToLabyrinthRow . lines $ content
                     where strToLabyrinthRow str = map charToSquare str
    return labyrinth
    
   
{-------------------------------------------------------------------
    Coordinate IO:
    Functions for getting start/end positions from command line.
 -------------------------------------------------------------------
-}
   
isValidInt :: String -> Bool                                     
isValidInt [] = False
isValidInt ['0'] = True
isValidInt ('-':xs) = isValidInt xs
isValidInt s = (all isDigit s) && (head s /= '0')
                                     
stringToInt :: String -> Maybe Int
stringToInt s | isValidInt s = Just (read s :: Int)
              | otherwise = Nothing
    
stringToPos :: String -> Maybe Pos2D
stringToPos s = 
    let ints  = map stringToInt $ words s
    in case ints of [] -> Nothing
                    [x] -> Nothing  -- one element only
                    (Nothing:_) -> Nothing
                    (_:Nothing:_) -> Nothing
                    (Just x:Just y:_) -> Just (x,y)

selectSquare :: String -> IO Pos2D                                      
selectSquare userPrompt = do
    putStrLn userPrompt
    startSq <- stringToPos <$> getLine
    case startSq of Nothing -> putStrLn "Invalid position. Try again." >> selectSquare userPrompt
                    Just x  -> return x
     
selectStart labyrinth = do 
    sq <- selectSquare "Give the start position as x y:"
    if isSquareFree sq labyrinth then return sq
    else putStrLn "Invalid position (either outside the labyrinth or blocked square. Try again)" >>
         selectStart labyrinth
                               
selectEnd labyrinth = do
    sq <-selectSquare "Give the end position as x y:"
    if isSquareFree sq labyrinth then return sq
    else putStrLn "Invalid position (either outside the labyrinth or blocked square. Try again)" >>
         selectEnd labyrinth       
    
    