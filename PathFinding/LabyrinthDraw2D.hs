import LabyrinthLib
import System.Environment
import Data.Char
import PathFinding
import Control.Monad
import Control.Applicative
import Graphics.Gloss                         


squareHeight :: Float        
squareHeight = 20


createIndexedList :: [a] -> [(Int,a)]
createIndexedList xs = zip [0..] xs

createIndexedList2D :: [[a]] -> [[((Int,Int),a)]]
createIndexedList2D xs =
    let createRowIndexes x = [(x,y) | y <- [0..]]
        rowIndexes = map createRowIndexes [0.. (length xs)]
     in map (\(x,y) -> zip x y) $ zip rowIndexes xs

     
drawLabyrinth :: Labyrinth -> Path2D -> Picture
drawLabyrinth (Labyrinth xs) path = 
    let indexed = createIndexedList2D xs
        drawSquare_ ((row,col),sq) = case sq of Open -> color blue $ drawSquare (row,col)
                                                Blocked -> color black $ drawSquare (row,col)
     in pictures $ map drawSquare_ $ concat indexed

drawPath :: Path2D -> Picture
drawPath path = pictures $ map (color red . drawSquare2) path
     
     
drawLabyrinth2 :: [(Int,Int)] -> Picture
drawLabyrinth2 blockedSquares = 
    pictures squares
    where squares = map (color red . drawSquare) blockedSquares

drawSquare :: (Int,Int) -> Picture        
drawSquare (row,col) =  translate (col_*squareHeight) (row_*squareHeight)
                        (Scale squareHeight squareHeight unitSquare)
                        where row_ = (-1)*fromIntegral row
                              col_ = fromIntegral col
                              
drawSquare2 :: (Int,Int) -> Picture        
drawSquare2 (row,col) = translate (col_*squareHeight + edgeLength/2) (row_*squareHeight + edgeLength/2)
                        (Scale edgeLength edgeLength unitSquare)
                        where row_ = (-1)*fromIntegral row
                              col_ = fromIntegral col
                              edgeLength = 0.6 * squareHeight

unitSquare :: Picture
unitSquare = Polygon [(0,0),(1,0),(1,1),(0,1)]


         
    
main = do
    args <- getArgs
    if length args == 0 then 
        error "Usage: LabyrinthDraw2D.exe <labyrinthFile>"
    else 
        do
        labyrinth <- readLabyrinthFromFile $ args!!0
        
        start <- selectStart labyrinth 
        end   <- selectEnd labyrinth
        
        let path = findPathInLabyrinth labyrinth start end
        animate (InWindow "Labyrinth" (500, 650) (20,  20))
                 black (\_ -> pictures [drawLabyrinth labyrinth path, drawPath path])


        

