import LabyrinthLib
import System.Environment
import Data.Char
import PathFinding
import Control.Monad
import Control.Applicative


solutionToStr ::Labyrinth -> Path2D -> [String]
solutionToStr (Labyrinth xs) path = 
    let rowIndexes = map (\i -> repeat i) [0..]
        colIndexes = repeat [0..]
        indexedData = zip3 rowIndexes colIndexes xs
        indexedData' = map (\(x,y,z) -> zip3 x y z) indexedData
        
        toChar (row,col,b) | (row,col) `elem` path = '*'
                           | otherwise = squareToChar b
                           
        rowToChars = map toChar
     in map rowToChars indexedData'
     
     
    
main = do
    args <- getArgs
    if length args == 0 then 
        error "Usage: LabyrinthDraw2D.exe <labyrinthFile>"
    else 
        do
        labyrinth <- readLabyrinthFromFile $ args!!0
        
        start <- selectStart labyrinth 
        end   <- selectEnd labyrinth
        
        let path = findPathInLabyrinth labyrinth start end
        let solution = solutionToStr labyrinth path
        
        mapM putStrLn solution