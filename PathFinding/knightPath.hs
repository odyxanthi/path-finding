import PathFinding
import qualified Data.Set
import qualified Data.List


data BoardPos = BoardPos Int Int deriving (Eq,Show,Ord)

outOfBounds :: BoardPos -> Bool
outOfBounds (BoardPos x y) = if x<1 || x>8 || y<1 || y>8 then True else False

board_diffx :: BoardPos -> BoardPos -> Int
board_diffx (BoardPos xstart _) (BoardPos xend _) = abs (xstart - xend)

board_diffy :: BoardPos -> BoardPos -> Int
board_diffy (BoardPos _ ystart) (BoardPos _ yend) = abs (ystart - yend)


    
data Knight = Knight BoardPos deriving Show

    
canKnightMoveTo :: BoardPos -> BoardPos -> Bool
canKnightMoveTo startPos endPos = 
    if outOfBounds endPos then False
    else (dx + dy == 3) && (dx==1 || dx == 2)
        where dx = board_diffx startPos endPos
              dy = board_diffy startPos endPos


knightMoves :: BoardPos -> [BoardPos]
knightMoves (BoardPos x y) = let isValid = canKnightMoveTo (BoardPos x y)
                             in filter isValid [(BoardPos xNew yNew) | xNew<-[(x-2)..(x+2)],yNew<-[(y-2)..(y+2)]]


knightPath start end = bfs start end knightMoves                             


validKnightPath :: [BoardPos] -> Bool
validKnightPath [] = True
validKnightPath [x] = True
validKnightPath (x:xs) = (head xs) `elem` (knightMoves x) && validKnightPath xs


main = do
     putStrLn "Specify start position as x y"
     str1 <- getLine
     let words1 = map (\s -> read s :: Int) $ words str1
     let start = BoardPos (words1!!0) (words1!!1)
     
     putStrLn "Specify end position as x y"
     str2 <- getLine
     let words2 = map (\s -> read s :: Int) $ words str2
     let end = BoardPos (words2!!0) (words2!!1)
     
     let p = knightPath start end
     print p
     putStrLn $ "is valid:" ++ (show $validKnightPath p)