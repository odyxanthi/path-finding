module PathFinding
(
    bfs
) where

import Data.Char
import qualified Data.Map as Map

type ParentMap a = Map.Map a a
type NeighborFunc a = (a->[a])
type DistanceFunc a = (a->Float)

-------------------------- BFS search ---------------------------------------------------------

{- Implementation of breadth first search
   start: the starting position
   end:   the position we are trying to discovered
   getNeighbors: function that given a position P, returns the positions directly reachable from P.
   returns: the positions that form the path from start to end.
-}
bfs :: Ord a => a -> a -> NeighborFunc a -> [a]
bfs start end getNeighbors = bfs_ (Map.singleton start start, [start]) end getNeighbors


{-  Recursive function that expands search front one position at a time until the 
    target position is discovered or the search front becomes empty.

    discovered: the discovered positions stored in a map, each state associated with its parent
                i.e. the position from which it was reached.
    searchFront: the positions that should be searched.
    dest:       the position that should be reached.
    getNeighbors: function that given a position P, returns the positions directly reachable from P.
-}
bfs_ :: Ord a => (ParentMap a,[a]) -> a -> NeighborFunc a -> [a]
bfs_ (discovered,searchFront) dest getNeighbors = 
    let (discovered',searchFront') = bfsExpand (discovered,searchFront) getNeighbors
    in  if dest `Map.member` discovered then
           pathToRoot discovered' dest []
        else
           bfs_ (discovered',searchFront') dest getNeighbors


{-  Expands the first position in the search front, and updates the search front 
    and the list of discovered states.
     
    Params:
    discovered: the discovered positions stored in a map, each state associated with its parent
                i.e. the position from which it was reached.
    searchFront: the positions that should be searched.
    getNeighbors: function that given a position P, returns the positions directly reachable from P.
-}
bfsExpand :: Ord a => (ParentMap a,[a]) -> NeighborFunc a -> (Map.Map a a, [a])
bfsExpand (discovered,searchFront) getNeighbors = 
    let currentPos = head searchFront
        newlyreached = filter (\x -> x `Map.member` discovered == False) $ getNeighbors currentPos

        -- traverse the newly reached and add them to the discovered wiht x as parent
        discovered' = foldl (\parentMap x -> Map.insert x currentPos parentMap) discovered newlyreached 
        searchFront' = (tail searchFront) ++ newlyreached
     in (discovered',searchFront')


pathToRoot :: Ord a => (ParentMap a) -> a -> [a] -> [a]
pathToRoot discovered start path = let parent = Map.lookup start discovered
                                in  case parent of Nothing -> (start:path) --error "parent = nothing"
                                                   Just x -> if x/=start then pathToRoot discovered x (start:path)
                                                             else (start:path)
-----------------------------------------------------------------------------------------------     



                                                             


